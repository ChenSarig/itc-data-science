"""multiplier function multiplies by 2 each letter of a giving string"""


def main():
    """main function receives a string from the user and implements
    the multiplier function on that string"""
    multiplier_str = raw_input("Please enter a string to multiply:  ")
    multi_result = multiplier(multiplier_str)
    print multi_result
    return multi_result


def multiplier(multiplier_str):
    """multiplier function receives a string from the main function
    and doubles each character in this string"""
    multi_result = ""
    for letter in multiplier_str:
        multi_result = multi_result + 2 * letter
    return multi_result


if __name__ == '__main__':
    main()
