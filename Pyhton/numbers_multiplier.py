"""numbers_multiplier multiples the number receives from the user
 and returns the result"""


def main():
    """ main function receives the users input and calls
     the num_multiply function  """

    num_to_multiply = raw_input("""Please enter a list of number to multiple,
      delimited by space :  """)
    multiply_result = num_multiply(num_to_multiply)
    print multiply_result
    return multiply_result


def num_multiply(num_to_multiply):
    """num_multiply function receives the users input from
     the main function and multiples those number"""
    numbers_2_multi = num_to_multiply.split()
    multiply_result = int(1)
    for num in numbers_2_multi:
        multiply_result = multiply_result * int(num)
    return multiply_result

if __name__ == '__main__':
    main()
