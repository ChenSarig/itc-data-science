"""samesame function sums the members of a list,
  with different types of variables:
  strings,integers or lists"""


def samesame(my_list):
    """samesame receives a list containing same or different variable.
     if all the element  """

    if all(isinstance(x, int)for x in my_list) or\
            all(isinstance(x, str)for x in my_list) or\
            all(isinstance(x, list) for x in my_list):
        same_sum = reduce((lambda x, y: x+y), my_list)
    else:
        same_sum = 'None'
    print same_sum
    return same_sum

# my_list = ['a', 'b', 'c']
# my_list = ['a', 'b' ,[1, 2], 3]
# my_list = [1, 2, 3, 4, 5, 6]
my_list = [[1, 2], [3, 4], [1, 2]]
# my_list = [1 ,2 ,[3 ,4]]
samesame(my_list)
