"""slicing_knife performs string slicing on the constant SLICING_PHRASE"""
SLICING_PHRASE = "We were more than just a slice"

def slicing_knife():
    """
     This function performs string slicing on the constant SLICING_PHRASE in
     4 different ways:

     slice_1 returns the first 5 characters of the string
     slice_2 returns the string from the 4 character till the last 2 (not including)
     slice_3 returns the all the odd characters of the string
     slice_4 returns the all the even characters of the string,
             starting from the 4th character
     slice_5 returns the characters in reverse order

      inputs: None
      outputs: 4 sliced strings
    """

    slice_1 = SLICING_PHRASE[:6]
    slice_2 = SLICING_PHRASE[4:-2]
    slice_3 = SLICING_PHRASE[::2]
    slice_4 = SLICING_PHRASE[3:-1:2]
    slice_5 = SLICING_PHRASE[::-1]

    print slice_1
    print slice_2
    print slice_3
    print slice_4
    print slice_5

slicing_knife()
